package socket;

import java.io.*;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private ServerSocket serverSocket;
    private List<PrintWriter> allout = new ArrayList<>();

    public Server(){
        try {
            System.out.println("正在启动服务端");
            serverSocket = new ServerSocket(8080);
            System.out.println("服务端启动完毕！");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){
        try {
            while(true){
                System.out.println("等待客户端连接...");
                Socket socket = serverSocket.accept();
                ClientHandler handler = new ClientHandler(socket);
                Thread t = new Thread(handler);
                t.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }

    private class ClientHandler implements Runnable{
        private Socket socket;
        private String host;

        public ClientHandler(Socket socket){
            this.socket = socket;
            host = socket.getInetAddress().getHostAddress();
        }

        @Override
        public void run() {
            PrintWriter pw = null;
            try {
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);

                // 通过socket获取输出流用于给客户端发送消息
                OutputStream out = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(out, StandardCharsets.UTF_8);
                BufferedWriter bw = new BufferedWriter(osw);
                pw = new PrintWriter(bw, true);

                // 将该输出流存入共享集合allOut
                synchronized (allout){
                    allout.add(pw);
                }
                System.out.println(host + "上线了，当前在线人数：" + allout.size());

                String message;

                while ((message = br.readLine()) != null){
                    System.out.println(host + "说：" + message);
                    // 将消息回复给所有客户端
                    synchronized (allout){
                        for(PrintWriter o : allout){
                            o.println(host + "说：" + message);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // 处理客户端断开后的操作
                synchronized (allout){
                    allout.remove(pw); // 将当前客户端输出流从共享集合中删除
                }

                System.out.println(host + "下线了，当前在线人数：" + allout.size());

                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
    }

}
